from django.shortcuts import render

# Create your views here.
from django.http import *
from django.template.loader import *

from .models import *

def index(request):
    a=Main.objects.all()[0]
    b=About.objects.all()[0]
    hard=HardSkill.objects.all()
    soft=SoftSkill.objects.all()
    pro=Project.objects.all()
  
    return render(request,'index.html',{
        'main':a,
        'abb':b,
        'hard':hard,
        'soft':soft,
        'pro':pro

      
    })