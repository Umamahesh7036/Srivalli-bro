from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(Main)
admin.site.register(About)
admin.site.register(HardSkill)
admin.site.register(SoftSkill)
admin.site.register(Project)