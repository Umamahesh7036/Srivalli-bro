from django.db import models

# Create your models here.

class Main(models.Model):

    first_name = models.CharField(max_length=30)
    last_name =  models.CharField(max_length=30)
    profession = models.CharField(max_length=30,default='Hero')
    picture = models.ImageField(upload_to='pics',default='assets/profile-pic.png')
    cv = models.FileField(upload_to='pics',default='assets/profile-pic.png')
    git_link =  models.CharField(max_length=300,default='Hero')
    linkedn_link =  models.CharField(max_length=300,default='Hero')
    gmail = models.CharField(max_length=30,default='jaibalayya@gmail.com')


class About(models.Model):

    picture = models.ImageField(upload_to='pics')
    about =  models.CharField(max_length=500)
    year = models.CharField(max_length=30)
    experience = models.CharField(max_length=30)
    edu1 = models.CharField(max_length=30)
    edu2 = models.CharField(max_length=30)



class HardSkill(models.Model):

     skill = models.CharField(max_length=30)
     level =  models.CharField(max_length=30)

class SoftSkill(models.Model):

     skill = models.CharField(max_length=30)
     level =  models.CharField(max_length=30)

class Project(models.Model):
     
     picture = models.ImageField(upload_to='pics')
     name = models.CharField(max_length=30)
     git_link = models.CharField(max_length=50)
     resource = models.FileField(upload_to='pics')
     
